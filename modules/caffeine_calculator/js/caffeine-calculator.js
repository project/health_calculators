(function ($, Drupal, drupalSettings) {

    'use strict';

    Drupal.behaviors.caffeineCalculator = {
      attach: function (context, settings) {
        // This code will run every time a part of the page is updated via Ajax.
        // 'context' is the part of the page which has been updated.

        // You can use jQuery to select and manipulate DOM elements, for example:
        // $('.my-element', context).addClass('my-class');
      },

      detach: function (context, settings, trigger) {
        // This function will be called when the 'context' is being removed from the page.
        // This could be useful to unbind event listeners to avoid memory leaks.
      }
    };

  })(jQuery, Drupal, drupalSettings);
