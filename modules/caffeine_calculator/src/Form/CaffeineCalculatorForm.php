<?php

namespace Drupal\caffeine_calculator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Caffeine calculator form.
 */
class CaffeineCalculatorForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'caffeine_calculator_caffeine_calculator';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'caffeine_calculator/caffeine_calculator';
    $config = $this->config('caffeine_calculator.settings');
    $drinks = $config->get('drinks');

    // If any of the required configurations are empty, display an error message.
    if (empty($drinks)) {
        $current_user = \Drupal::currentUser();
        $link = '';

        // If the user is UID 1 or has the right permissions, provide a link to the settings page.
        if ($current_user->id() == 1 || $current_user->hasPermission('Administer Caffeine Calculator settings')) {
            $url = \Drupal\Core\Url::fromRoute('caffeine_calculator.settings_form');
            $link = ' ' . $this->t('<a href=":url" target="_blank">Configure it here</a>.', [':url' => $url->toString()]);
        }

        $form['error'] = [
            '#type' => 'markup',
            '#markup' => $this->t('The caffeine calculator must be configured to use it.') . $link,
            '#prefix' => '<div class="messages messages--error">',
            '#suffix' => '</div>',
        ];
        return $form;
    }

    // Help text
    $help_text = $config->get('help_text');
    if (!empty($help_text)) {
      $form['help_text'] = [
        '#type' => 'markup',
        '#markup' => '<p>' . $this->t($help_text) . '</p>',
        '#weight' => -10,
      ];
    }

    $drinks_config = $config->get('drinks');
    $options = ['' => $this->t('Select Drink')];
    $caffeine_amounts = [];

    if (is_array($drinks_config)) {
        foreach ($drinks_config as $drink) {
            $name = $drink["drink_name"];
            $amount = $drink["caffeine_amount"];
            $options[$name] = $name;
            $caffeine_amounts[$name] = $amount;
        }
    }

    // Drink name list
    $form['drink_name'] = [
        '#type' => 'select',
        '#title' => $this->t('Drink'),
        '#required' => TRUE,
        '#options' => $options,
        '#ajax' => [
            'callback' => '::updateCaffeineAmount',
            'wrapper' => 'caffeine-amount-wrapper',
        ],
    ];

    $selected_drink = $form_state->getValue('drink_name');
    $amount = isset($caffeine_amounts[$selected_drink]) ? $caffeine_amounts[$selected_drink] : '';

    $markup = $selected_drink ? '<div class="caffeine-amount-wrapper-style">'.$this->t('Caffeine amount in the selected drink: %amount mg/100ml', ['%amount' => $amount]).'</div>' : '';

    // Wrapper to display the caffeine Amount of the selected drink
    $form['caffeine_amount'] = [
        '#type' => 'markup',
        '#prefix' => '<div id="caffeine-amount-wrapper">',
        '#suffix' => '</div>',
        '#markup' => $markup,
    ];

    // Drink Amount
    $form['drink_amount'] = [
        '#type' => 'number',
        '#title' => $this->t('Drink amount (ml)'),
        '#min' => 1,
        '#required' => TRUE,
        '#description' => $this->t('Enter the amount of drink in milliliters.'),
    ];

    // Age range
    $form['age_range'] = [
      '#type' => 'select',
      '#title' => $this->t('Age Range'),
      '#options' => [
        '3 - 18' => $this->t('3 - 18'),
        '19 - 65' => $this->t('19 - 65'),
      ],
      '#required' => TRUE,
    ];

    // Medical conditions
    $form['medical_conditions'] = [
      '#type' => 'radios',
      '#title' => $this->t('Medical Conditions'),
      '#options' => [
        'none' => $this->t('None'),
        'pregnant' => $this->t('Pregnant and lactating women'),
        'cardiovascular' => $this->t('Cardiovascular patients'),
      ],
      '#required' => TRUE,
    ];

    // Weight and unit
    $form['weight_elements_container'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['weight-elements-container']],
    ];

    $form['weight_elements_container']['weight'] = [
      '#type' => 'number',
      '#title' => $this->t('Weight'),
      '#min' => 1,
      '#size' => 255,
      '#required' => TRUE,
      '#prefix' => '<div class="weight-element-flex">',
      '#suffix' => '</div>',
    ];

    $form['weight_elements_container']['weight_unit'] = [
      '#type' => 'radios',
      '#options' => [
        'kg' => $this->t('KG'),
        'lb' => $this->t('LB'),
      ],
      '#default_value' => 'kg',
      '#prefix' => '<div class="weight-element-flex">',
      '#suffix' => '</div>',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Calculate'),
      '#ajax' => [
        'callback' => '::ajaxResponse',
        'wrapper' => 'result-message-wrapper',
      ],
    ];

    $form['result_message'] = [
      '#type' => 'markup',
      '#prefix' => '<div id="result-message-wrapper">',
      '#suffix' => '</div>',
      '#markup' => '',  // This will be filled in by the AJAX callback.
    ];

    return $form;
  }

  /**
   * Ajax callback to update the caffeine amount display.
   */
  public function updateCaffeineAmount(array $form, FormStateInterface $form_state) {
    return $form['caffeine_amount'];
  }

  /**
   * Ajax response for the form submission.
   */
  public function ajaxResponse(array $form, FormStateInterface $form_state) {
    // Fetch the messages stored in form_state storage.
    $message_1 = $form_state->getStorage()['result_message_1'] ?? '';
    $message_2 = $form_state->getStorage()['result_message_2'] ?? '';

    // Combine both messages.
    $response = $message_1 . '<br/>' . $message_2;

    // Update the form's result_message markup with the fetched messages.
    $form['result_message']['#markup'] = '<div class="caffeine-amount-wrapper-style">'.$response.'</div>';

    return $form['result_message'];  // Return just the 'result_message' part of the form.
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Add any validation if needed.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Age, weight, and medical conditions
    $age_range = $form_state->getValue('age_range');
    $weight = $form_state->getValue('weight');
    $weight_unit = $form_state->getValue('weight_unit');
    $medical_conditions = $form_state->getValue('medical_conditions');

    // If the weight is in pounds, convert it to kilograms before using it
    if ($weight_unit == 'lb') {
      $weight = $weight * 0.45359237;
    }

    $caffeine_amount = 0;
    if ($age_range === '3 - 18') {
      $caffeine_amount = $weight * 2.5;
    } elseif ($age_range === '19 - 65') {
      if ($medical_conditions === 'pregnant') {
        $caffeine_amount = 300;
      } elseif ($medical_conditions === 'cardiovascular') {
        $caffeine_amount = 200;
      } else {
        $caffeine_amount = 400;
      }
    }

    // Drinks
    $drink_name = $form_state->getValue('drink_name');
    $drink_amount = $form_state->getValue('drink_amount');

    $config = $this->config('caffeine_calculator.settings');
    $drinks_config = $config->get('drinks');

    $caffeine_content_per_100ml = 0;
    foreach ($drinks_config as $drink) {
      if ($drink['drink_name'] == $drink_name) {
        $caffeine_content_per_100ml = $drink['caffeine_amount'];
        break;
      }
    }

    $total_caffeine = ($caffeine_content_per_100ml * $drink_amount) / 100;
    $result_message_1 = $this->t('The daily caffeine amount for you = @amount mg', ['@amount' => number_format($caffeine_amount, 2)]);
    $result_message_2 = $this->t('The caffeine amount in your drink = @amount mg', ['@amount' => number_format($total_caffeine, 2)]);

    // Store the result messages in form_state storage so that we can fetch it in the AJAX callback.
    $form_state->setStorage(['result_message_1' => $result_message_1, 'result_message_2' => $result_message_2]);
  }



}
