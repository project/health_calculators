<?php

namespace Drupal\caffeine_calculator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Caffeine Calculator settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'caffeine_calculator_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['caffeine_calculator.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('caffeine_calculator.settings');

    $form['help_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Help Text'),
      '#description' => $this->t('Enter help text that will be shown on the Caffeine Calculator form.'),
      '#default_value' => $config->get('help_text'),
    ];

    $drinks = $config->get('drinks') ?? [];
    $drinks_value = '';
    foreach ($drinks as $drink) {
      $drinks_value .= "{$drink['drink_name']}|{$drink['caffeine_amount']}\n";
    }

    $form['drinks_list'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Drinks and Caffeine Amounts'),
      '#required' => TRUE,
      '#description' => $this->t('Enter each drink and its caffeine amount per 100ml in the format: drink_name|caffeine_amount. One item per line.'),
      '#default_value' => $drinks_value,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $drinks_list = $form_state->getValue('drinks_list');
    $drinks_lines = explode("\n", $drinks_list);

    foreach ($drinks_lines as $line) {
      if (trim($line) && count(explode('|', $line)) !== 2) {
        $form_state->setErrorByName('drinks_list', $this->t('Make sure each line is in the format: drink_name|caffeine_amount.'));
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $drinks_list = $form_state->getValue('drinks_list');
    $drinks_lines = explode("\n", $drinks_list);
    $drinks = [];

    foreach ($drinks_lines as $line) {
      if (trim($line)) {
        list($name, $amount) = explode('|', $line);
        $drinks[] = ['drink_name' => trim($name), 'caffeine_amount' => trim($amount)];
      }
    }

    $this->config('caffeine_calculator.settings')
      ->set('drinks', $drinks)
      ->set('help_text', $form_state->getValue('help_text'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}